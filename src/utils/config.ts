export const SITE_TITLE = 'Malo Polese'
export const SITE_DESCRIPTION =
  'Hello, my name is Malo. I am a Full-Stack / DevOps engineer and I work at Conduktor'
export const SITE_LANG = 'en'
