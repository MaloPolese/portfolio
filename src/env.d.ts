declare namespace App {
  interface Locals {
    title: string
  }
}

interface ImportMetaEnv {
  readonly STRAPI_URL: string
  readonly STRAPI_TOKEN: string
}
