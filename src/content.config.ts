import { defineCollection } from 'astro:content'
import fetchApi from './lib/strapi'
import { z } from 'astro:schema'
import type Local from './interfaces/local'

const locales = defineCollection({
  loader: async () => {
    const locales = await fetchApi<Local[]>({
      endpoint: 'i18n/locales',
      query: {
        populate: '*',
      },
    })
    return locales.map(({ id, ...locale }) => ({
      id: id.toString(),
      ...locale,
    }))
  },
  schema: z.object({
    id: z.string(),
    documentId: z.string(),
    name: z.string(),
    code: z.string(),
    createdAt: z.string(),
    updatedAt: z.string(),
    isDefault: z.boolean(),
  }),
})

export const collections = { locales }
