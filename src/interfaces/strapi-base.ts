export type StrapiBase = {
  id: number
  documentId: string
  createdAt: string
  updatedAt: string
}
